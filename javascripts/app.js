function btnStartClickaudio() {
    console.log('btnStartClickaudio');
    navigator.getUserMedia(
        {video: false , audio: true},
function (Stream){
    console.log('successCallback',Stream);
    var audio = document.getElementById('audio');
    audio.src = window.URL.createObjectURL(Stream);
    audio.play();
    playingAudioStream = Stream;
        },
        errorCallback
    );
}

function btnStopClickaudio() {
    console.log('btnStopClickaudio');
    playingAudioStream.getTracks().forEach(function(t) {
        console.log(t);
        t.stop();
    });
}

var isMute = false;
function btnToggleMuteClickaudio(){
    var audio = document.getElementById('audio');
    var btnPause = document.getElementById('btnToggleMute');
    if(!isMute){
        audio.pause();
        btnToggleMute.textContent = 'Unmute';
    }
    else{
        audio.play();
        btnToggleMute.textContent = 'Mute';
    }
    isMute = !isMute;
}
var playingVideoStream = null;
var playingAudioStream = null;
function btnStartClickvideo() {
    console.log('btnStartClickvideo');
    navigator.getUserMedia(
        {video: true , audio: false},
function (Stream){
    console.log('successCallback',Stream);
    var video = document.getElementById('video');
    video.src = window.URL.createObjectURL(Stream);
    video.play();
    playingVideoStream = Stream;
        },
        errorCallback
    );
}

function btnStopClickvideo() {
    console.log('btnStopClickvideo');
    playingVideoStream.getTracks().forEach(function(t) {
        console.log(t);
        t.stop();
    });
}

var isPause = false;
function btnTogglePauseClickvideo(){
    var video = document.getElementById('video');
    var btnPause = document.getElementById('btnTogglePause');
    if(!isPause){
        video.pause();
        btnTogglePause.textContent = 'UnPause';
    }
    else{
        video.play();
        btnTogglePause.textContent = 'Pause';
    }
    isPause = !isPause;
}

function errorCallback(err){
    console.log('errorCallback',err);
}